'use strict';

var gulp = require('gulp'),
  watch = require('gulp-watch'),
  prefixer = require('gulp-autoprefixer'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  include = require("gulp-include"),
  rimraf = require('rimraf'),
  browserSync = require("browser-sync"),
  plumber = require('gulp-plumber'),
  size = require('gulp-size'),
  minifyCSS = require('gulp-minify-css'),
  reload = browserSync.reload;


var path = {
  build: { //Тут мы укажем куда складывать готовые после сборки файлы
    html: 'app/',
    js: 'app/js/',
    css: 'app/css/',
    img: 'app/img/',
    svg: 'src/svg/',
    fonts: 'app/fonts/'
  },
  src: { //Пути откуда брать исходники
    html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
    js: 'src/js/*.js',//В стилях и скриптах нам понадобятся только main файлы
    style: 'src/style/*.scss',
    img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
    svg: 'src/svg/*.svg',
    fonts: 'src/fonts/**/*.*'
  },
  watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    style: 'src/style/**/*.scss',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  clean: './app'
};

var config = {
  server: {
    baseDir: "./app"
  },
  tunnel: false,
  host: 'localhost',
  port: 9000,
  logPrefix: "Frontend"
  //browser: "firefox"
};

gulp.task('html:build', function () {
  gulp.src(path.src.html) //Выберем файлы по нужному пути
    .pipe(include()) //Прогоним через include
    .pipe(plumber())
    .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
    .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
  gulp.src(path.src.js) //Найдем наш main файл
    .pipe(plumber())
    //.pipe(sourcemaps.init()) //Инициализируем sourcemap
    .pipe(include()) //Прогоним через include
    //.pipe(sourcemaps.write({includeContent: true})) //Пропишем карты
    .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
    .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('style:build', function () {
  gulp.src(path.src.style) //Выберем наш main.scss
    .pipe(plumber())
    //.pipe(sourcemaps.init()) //То же самое что и с js
    .pipe(sass()) //Скомпилируем
    .pipe(prefixer()) //Добавим вендорные префиксы
    .pipe(minifyCSS())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.css)) //И в build
    .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    return gulp.src([path.src.img, '!src/imag/**/*.db'])
      .pipe(gulp.dest(path.build.img));
});

gulp.task('fonts:build', function() {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
  'html:build',
  'js:build',
  'style:build',
  'fonts:build',
  'image:build'
]);

gulp.task('watch', function(){
  watch([path.watch.html], function(event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.img], function(event, cb) {
    gulp.start('image:build');
  });
  watch([path.watch.fonts], function(event, cb) {
    gulp.start('fonts:build');
  });
});

gulp.task('webserver', function () {
  browserSync(config);
});

gulp.task('clean', function (cb) {
  rimraf(path.clean, cb);
});

gulp.task('start', ['build', 'webserver', 'watch']);
