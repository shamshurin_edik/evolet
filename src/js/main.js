//=require libs/jquery.js
//=require libs/slick.js
//=require libs/parallax.js


var h_hght = 100; // РІС‹СЃРѕС‚Р° С€Р°РїРєРё
var h_mrg = 0;    // РѕС‚СЃС‚СѓРї РєРѕРіРґР° С€Р°РїРєР° СѓР¶Рµ РЅРµ РІРёРґРЅР°
                 
$(function(){
 
    var elem = $('#header');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.addClass('dark');
        $('.logo_a img').attr('src', 'img/logo-dark.svg');
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.removeClass('dark');
            $('.logo_a img').attr('src', 'img/logo.svg');

        } else {
        	elem.addClass('dark');
            $('.logo_a img').attr('src', 'img/logo-dark.svg');

        }
    });
 
});


function toggle_nav() {
  $('#header').toggleClass('visible');
}

$('.file_input').change(function() {
  //находим родительский блок с классом file_select (обертка кнопки)
  var wrap = $(this).parents('.file_select');
  var filename = $(this).val();
  if (filename) {
    if (filename.substring(3,11) == 'fakepath') {
      filename = filename.substring(12);
    }
    //выводим в блоки соответствующими классами информацию о файле
    $(wrap).find(".file_name").html(filename);
    $(wrap).find(".file_wheight").html(Math.round(this.files[0].size/1024) + "Kb");
    //добавляем к родительскому блоку класс selected
    $(wrap).find(".file_label").addClass('selected');
  }
});



function submit_form(e) {
  var form = e.getElementsByClassName('form')[0];
  var buttons = e.getElementsByClassName('buttons')[0];
  if (buttons) {
    $(buttons).addClass('submited');
  }
  $(form).addClass('submited');
}

function reset_form(e, text) {
  var form = $(e).parents('form');
  var form_wrap = $(e).parents('.form');
  var buttons = $(form).find('.buttons');
  if (text) {    
    $(form).find(".file_name").html(text);
    $(form).find(".file_label").removeClass('selected');
  }
  $(buttons).removeClass('submited');
  $(form_wrap).removeClass('submited');
}